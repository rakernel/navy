if (window!=top){top.location.href = window.location.href;}

// next or prev page
// finding native solution instead lol
var prevtitle = $("#thePrev > a").attr("title"),
    nexttitle = $("#theNext > a").attr("title"),
    prevlink = $("#thePrev > a").attr("href"),
    nextlink = $("#theNext > a").attr("href");

$("#post-navigator > span").remove();

$("a.post-navigator-prev").attr("href", prevlink).append(prevtitle);
$("a.post-navigator-next").attr("href", nextlink).prepend(nexttitle);

// auto pull down in mobile
if ( $(window).width() < 1024 ) {
	$(document).ready(function(){
		$("body, html").scrollTop(300);
	});
}
