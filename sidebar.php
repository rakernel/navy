<div id="secondary">
  <header class="sidebar-head">
    <div class="sidebar-head-content">
      <i class="author-avatar">
        <?php if ( $this->options->navy_avatar ): ?>
          <img src="<?php echo $this->options->navy_avatar ?>"></img>
        <?php else: ?>
          <img src="<?php echo getGravatar($this->options->navy_gravatar, 160); ?>"></img>
        <?php endif; ?>
      </i>
      <h1 class="author-title"><?php echo $this->options->navy_author ?></h1><br>
      <p class="author-description"><?php echo $this->options->navy_tips ?></p>
    </div>
  </header>
  <div class="sidebar">
    <section class="widget head-lt">
      <ul class="widget-list widget-list2">

        <li class="meta-quat">
          <h3><a href="https://github.com/rakernel" target="__blank">
            <i class="icon-github"></i>
          </a></h3>
        </li>
        <li class="meta-quat">
          <h3><a href="http://weibo.com/niverntin" target="__blank">
            <i class="icon-weibo"></i>
          </a></h3>
        </li>

      </ul>
    </section>
    <section class="widget head-rb">
      <form id="search" method="post" action"./">
        <input type="search" name="s" class="text" placeholder='find / -name "type"'></input>
        <button type="submit" class="submit icon-enter" title="Run!"></button>
      </form>
    </section>
    <?php if ( !empty($this->options->sidebarBlock) && in_array('ShowMeta', $this->options->sidebarBlock) ): ?>
      <section class="widget widget-fixed head-iv">
        <h3 class="widget-title">
          META:
        </h3>
        <ul class="widget-list widget-list2">
          <?php if ( $this->user->hasLogin() ): ?>
            <li>
              <h3><a href="<?php $this->options->logoutUrl(); ?>">
                Log out...
              </a></h3>
            </li>
          <?php endif; ?>
          <li>
            <h3><a id="about" href="/about.html" title="一名不懂是非的小屁宅">
              About / 关于
            </a></h3>
          </li>
          <li class="meta-quat" style="float: right;">
            <h3><a href="<?php $this->options->feedUrl(); ?>">
              <i class="icon-rss"></i>
            </a></h3>
          </li>
          <li class="meta-quat" style="float: right; text-align: center;">
            <h3><a href="https://zh.wikipedia.org/wiki/Wikipedia:CC_BY-SA_3.0%E5%8D%8F%E8%AE%AE%E6%96%87%E6%9C%AC" title="遵循CC BY-SA 3.0协议">
              <i class="icon-cc"></i>
            </a></h3>
          </li>
          <li class="meta-half">
            <h3><a href="http://www.typecho.org">
              Typecho )))
            </a></h3>
          </li>
        </ul>
      </section>
    <?php endif; ?>
  </div>
</div>
