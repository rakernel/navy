<?php
/**
 * Just a theme...
 * @package Navy
 **/

$this->need('header.php');

?>

<div class="col-8" id="content">
  <div class="res-cons">
    <?php while( $this->next() ): ?>
      <article class="post">
        <div class="post-meta">
          <?php $this->date('F j, Y'); ?>
          <section class="tags" itemprop="keywords">
            <?php if ( $this->tags ): ?>
              <i class="icon-tags"></i>
              #<?php $this->tags(', #', true, 'none'); ?>
            <?php endif; ?>
          </section>
        </div>
        <header>
          <h2 class="post-title">
            <a href="<?php $this->permalink(); ?>#content">
              <?php $this->title(); ?>
            </a>
          </h2>
        </header>
        <div class="post-content post-excerpt">
          <?php $this->excerpt(240, ' ...<p class="more"><a href="'.$this->permalink.'#content">'.$this->options->navy_continue.'</a></p>'); ?>
        </div>
      </article>
    <?php endwhile; ?>
    <?php $this->pageNav('&laquo; 回望', '前行 &raquo;', 10, '...'); ?>
  </div>
</div>

<?php $this->need('footer.php'); ?>
