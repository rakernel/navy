#Typecho ))) Theme Navy
__Just a simple theme__
__Original author is [cho](http://chopstack.com/)__

#Install
1. cd /path/to/typecho/usr/themes
2. git clone
3. enable in setting :)

#Spark
1. 焕然一新的设计（其实跟原版差不多啦。。。）
2. disqus评论（也可以自行换成多说）
3. 部分自适应（还在完善，求改进）
4. 优化了原主题的结构等

#Customize
1. 所有的标签必须自行在sidebar.php中更改（可能会考虑加入设置项中）
2. 头像支持gravatar，也可以自行上传之后使用（两者同开的话优先考虑上传的）
3. hitokoto.us支持（仅在页面，也可以自行修改）

暂时就这么多。

P.S. 原谅一个边“抄袭”边学习php的小学生吧（默念php是最好的语言）
