<?php

function themeConfig($form) {

  $navy_gravatar = new Typecho_Widget_Helper_Form_Element_Text(
    'navy_gravatar', NULL, '', _t('头像'), _t('您的Gravatar头像')
  );
  $navy_gravatar->input->setAttribute('class', 'w-100 mono');
  $form->addInput( $navy_gravatar->addRule('email', 'Gravatar邮箱') );

  $navy_avatar = new Typecho_Widget_Helper_Form_Element_Text(
    'navy_avatar', NULL, '', _t('头像链接'), _t('如果没有Gravatar的话')
  );
  $form->addInput( $navy_avatar );

  $navy_tips = new Typecho_Widget_Helper_Form_Element_Text(
    'navy_author', NULL, '', _t('昵称'), _t('侧边栏显示')
  );
  $form->addInput( $navy_tips );

  $navy_tips = new Typecho_Widget_Helper_Form_Element_Text(
    'navy_tips', NULL, '', _t('简介'), _t('侧边栏显示')
  );
  $form->addInput( $navy_tips );

  $navy_continue = new Typecho_Widget_Helper_Form_Element_Text(
    'navy_continue', NULL, '继续阅读', _t('继续阅读'), _t('默认为 继续阅读 ')
  );
  $form->addInput( $navy_continue );

  $sidebarBlock = new Typecho_Widget_Helper_Form_Element_Checkbox('sidebarBlock',array(
    'ShowMeta' => _t('显示META')), _t('侧边栏显示')
  );
  $form->addInput( $sidebarBlock->multiMode() );

}

function getGravatar( $email, $s = 40, $d = 'mm', $g = 'g' ) {
  $hash = md5( $email );
  $gavatar = "https://secure.gravatar.com/avatar/$hash?s=$s&d=$d&r=$g";
  return $gavatar;
}
