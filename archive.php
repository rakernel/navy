<?php $this->need('header.php'); ?>

<div class="col-8" id="content">
  <div class="res-cons">
    <h3 class="archive-title">
      <?php $this->archiveTitle(array(
        'category'  =>  _t('Category: %s'),
        'search'    =>  _t('Seach: %s'),
        'tag'       =>  _t('Tag: %s'),
        'author'    =>  _t('Author: %s')
      ), '', ''); ?>
    </h3>
    <?php if ( $this->have() ): ?>
      <?php while( $this->next() ): ?>
        <article class="post">
          <date class="post-meta">
            <?php $this->date('F j, Y'); ?>
          </date>
          <header>
            <h2 class="post-title">
              <a href="<?php $this->permalink(); ?>#content"><?php $this->title(); ?></a>
            </h2>
          </header>
          <div class="post-content">
            <?php $this->content(); ?>
          </div>
        </article>
      <?php endwhile; ?>
    <?php else: ?>
      <article class="post">
        <h2 class="post-title">
          空。
        </h2>
      </article>
    <?php endif; ?>
    <?php $this->pageNav('&laquo; 前行', '回望 &raquo;'); ?>
  </div>
</div>

<?php $this->need('footer.php'); ?>
