<!DOCTYPE HTML>
<html>

<head>
  <meta charset="<?php $this->options->charset(); ?>" />
  <link rel="shortcut icon" href="<?php $this->options->themeUrl('favicon.png'); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="MobileOptimized" content="320" />
  <meta name="HandheldFriendly" content="True" />
  <title>
    <?php $this->archiveTitle(array(
      'category'  =>  _t('Category: %s'),
      'search'    =>  _t('Seach: %s'),
      'tag'       =>  _t('Tag: %s'),
      'author'    =>  _t('Author: %s')
    ), '', ' - '); ?><?php $this->options->title(); ?>
  </title>
  <?php $this->header("generator=&template=&"); ?>

  <?php /* stylesheet and some js */ ?>
  <link rel="stylesheet" href="<?php $this->options->themeUrl('css/normalize.css'); ?>" />
  <link rel="stylesheet" href="<?php $this->options->themeUrl('css/navy.css'); ?>" />
  <link rel="stylesheet" href="https://fonts.lug.ustc.edu.cn/css?family=Open+Sans:400,700" />
  <link rel="stylesheet" href="https://fonts.lug.ustc.edu.cn/css?family=Raleway:400" />
  <link rel="stylesheet" href="https://fonts.lug.ustc.edu.cn/css?family=Source+Code+Pro:300" />
  <link rel="stylesheet" href="<?php $this->options->themeUrl('css/icons.css'); ?>" />

  <script type="text/javascript" src="<?php $this->options->themeUrl('js/jquery.min.js'); ?>"></script>

</head>

<body>

  <?php $this->need('sidebar.php'); ?>

  <div class="move-block">

    <header id="header" class="clearfix">
      <div class="container">
        <div class="col-group">
          <div class="site-name">
            <?php if ( $this->is('index') ): ?>
              <a id="logo" href="<?php $this->options->siteUrl(); ?>">
                <?php $this->options->title(); ?>
              </a>
              <h1>
                <?php $this->options->title(); ?>
              </h1>
            <?php else: ?>
              <a id="logo" href="<?php $this->options->siteUrl(); ?>">
                <?php $this->options->title(); ?>
              </a>
            <?php endif; ?>
            <p class="description">
              <?php $this->options->description(); ?>
            </p>
          </div>
          <div>
            <nav id="nav-menu" class="clearfix">
              <a class="<?php if ( $this->is('index') ): ?>current<?php endif; ?> <?php if ( $this->is('post') ): ?>current<?php endif; ?>" href="<?php $this->options->siteUrl(); ?>">
                <?php _e('Index'); ?>
              </a>
              <?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
              <?php while( $pages->next() ): ?>
                <a <?php if ( $this->is('page', $pages->slug) ): ?>class="current"<?php endif; ?>href="<?php $pages->permalink(); ?>" title="<?php $pages->title(); ?>"><?php $pages->title(); ?></a>
              <?php endwhile; ?>
            </nav>
          </div>
        </div>
      </div>
    </header>

  <div id="body">
    <div class="container">
      <div class="col-group">
        <?php /* end with footer.php */ ?>
