<?php $this->need('header.php'); ?>

<div class="col-8" id="content">
  <div class="res-cons">
    <article class="post">
      <header>
        <h1 class="post-title">
          <?php $this->title() ?>
        </h1>
      </header>
      <div class="page-content post-content">
        <?php $this->content(); ?>
      </div>
      <footer class="post-footer">
        <section class="navy_foot">
          <p>
            <a class="icon-rquo"></a>
            <script type="text/javascript" src="//io.runlevel.org/api.hitokoto.us/rand?encode=js"></script>
            <script>hitokoto();</script>
          </p>
        </section>
      </footer>
    </article>
    <?php if ( $this->allow('comment') ): ?>
      <?php $this->need('comments.php'); ?>
    <?php endif; ?>
  </div>
</div>

<?php $this->need('footer.php'); ?>
