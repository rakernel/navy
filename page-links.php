<?php

/**
 * links
 * @package custom
 **/

$this->need('header.php'); ?>
<link rel="stylesheet" href="<?php $this->options->themeUrl('css/links.css'); ?>" />

<div class="col-8" id="content">
  <div class="res-cons">
    <article class="post">
      <header>
       <h1 class="post-title">
         <?php $this->title() ?>
       </h1>
      </header>
     <div class="page-content post-content">
       <nav class="links">
         <?php $this->content(); ?>
       </nav>
       <div class="request-links">
         <p>[ <a href="/request-link.html" title="做个好朋友吧~ lol" target="_blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=yes,height=600,width=500');return false;">
           REQUEST LINKS
         </a> ]</p>
       </div>
     </div>
     <footer class="post-footer">
       <section class="navy_foot">
         <p>
           <a class="icon-rquo"></a>
           <script type="text/javascript" src="//io.runlevel.org/api.hitokoto.us/rand?encode=js"></script>
           <script>hitokoto();</script>
         </p>
       </section>
     </footer>
   </article>
 </div>
</div>

<?php $this->need('footer.php'); ?>
