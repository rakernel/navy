<div id="comments">
  <?php $this->comments()->to($comments); ?>
  <?php if ( $comments->have() ): ?>
    <span class="widget-title">
      <?php $this->commentsNum(_t('0 echo'), _t('1 echo'), _t('%d echos')); ?>
    </span>
    <?php $comments->listComments(); ?>
    <?php $comments->pageNav('&laquo;', '&raquo;'); ?>
  <?php endif; ?>

  <?php if ( $this->allow('comment') ): ?>
    <div id="<?php $this->respondID(); ?>" class="respond">
      <div class="cancel-comment-reply">
        <?php $comments->cancelReply(); ?>
      </div>
      <span id="response" class="widget-title">
        Your echo:
      </span>
      <form method="post" action="<?php $this->commentUrl(); ?>" id="comment-form">
        <div class="col1">
          <p>
            <textarea rows="8" cols="50" name="text" class="textarea"><?php $this->remember('text'); ?></textarea>
          </p>
        </div>
        <div class="col2">
          <?php if ( $this->user->hasLogin() ): ?>
            <p style="text-align: center;">
              Hello, <a href="<?php $this->options->profileUrl(); ?>"><?php $this->user->screenName(); ?></a> !
            </p>
          <?php else: ?>
            <p>
              <label for="author" class="required">You are:</label>
              <input type="text" name="author" id="author" class="text" value="<?php $this->remeber('author'); ?>"></input>
            </p>
            <p>
              <label for="mail" <?php if ( $this->options->commentsRequireMail ): ?>class="required"<?php endif; ?>>Email:</label>
              <input type="email" name="mail" id="mail" class="text" value="<?php $this->remember('mail'); ?>"></input>
            </p>
            <p>
              <label for="url" <?php if ( $this->options->commentsRequireURL ): ?>class="required"<?php endif; ?>>Your site:</label>
              <input type="url" name="url" id="url" class="text" placeholder="http://example.com" value="<?php $this->remember('url'); ?>"></input>
            </p>
          <?php endif; ?>
          <p>
            <button type="submit" class="submit">SEND YOUR ECHO :)</button>
          </p>
        </div>
      </form>
    </div>
  <?php else: ?>
  <?php endif; ?>
</div>
