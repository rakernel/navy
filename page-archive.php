<?php 

/**
 * archives
 * @package custom
 **/

$this->need('header.php'); ?>

<div class="col-8" id="content">
  <div class="res-cons">
    <article class=“post”>
      <div class="post-content-pages">
        <?php
        $this->widget('Widget_Contents_Post_Recent', 'pageSize=10240')->to($archives);
        $year = 0; $mon = 0; $i = 0; $j = 0;
        $output = '<div id="archives">';
        while( $archives -> next() ):
          $year_tmp = date('Y', $archives->created);
          if ( $year != $year_tmp && $year_tmp > 0 ) $output .= '</ul>';
          if ( $year != $year_tmp ) {
            $year = $year_tmp;
            $output .= '<div class="al_year">'.$year.'</div><ul class="al_mon_list">';
          }
          $output .= '<li>'.date('m /d', $archives->created).'<a href="'.$archives->permalink.'#content">'.$archives->title.'</a></li>';
        endwhile;
        $output .= '</ul></li></ul></div>';
        echo $output;
        ?>
      </div>
    </article>
    <footer class="post-footer">
      <section class="navy_foot">
        <p>
          <a class="icon-rquo"></a>
          <script type="text/javascript" src="//io.runlevel.org/api.hitokoto.us/rand?encode=js"></script>
          <script>hitokoto();</script>
        </p>
      </section>
    </footer>
  </div>
</div>

<?php $this->need('footer.php'); ?>
