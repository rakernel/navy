<?php $this->need('header.php'); ?>

<div class="col-8" id="content">
  <div class="res-cons">
    <article class="post">
      <div class="post-meta">
        <?php $this->date('F j, Y'); ?>
        <section class="tags" itemprop="keywords">
          <?php if ( $this->tags ): ?>
            <i class="icon-tags"></i>
            #<?php $this->tags(', #', true, 'none'); ?>
          <?php endif; ?>
        </section>
      </div>
      <header>
        <h1 class="post-title">
          <?php $this->title(); ?>
        </h1>
      </header>
      <div class="post-content">
        <?php $this->content(); ?>
      </div>
    </article>
    <footer class="post-footer">
      <section class="navy_foot">
        <p id="post-navigator">
          <a class="post-navigator post-navigator-prev"><i class="icon-larr"></i></a>
          <span id="thePrev"><?php $this->thePrev('%s', NULL, array('title' => ' ')); ?></span>
          <a class="post-navigator post-navigator-next"><i class="icon-rarr"></i></a>
          <span id="theNext"><?php $this->theNext('%s', NULL, array('title' => ' ')); ?></span>
          </span>
        </p>
      </section>
    </footer>
    <?php if ( $this->allow('comment') ): ?>
      <?php $this->need('comments.php'); ?>
    <?php endif; ?>
  </div>
</div>

<?php $this->need('footer.php'); ?>
